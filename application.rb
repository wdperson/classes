require_relative 'models/organization'

class Application

  def initialize
    create
  end

  def create
    Organization.new('Root Org', nil, 'Bob', 'admin').create
    Organization.new('Org 1', 'root', 'Ed', 'denied').create
  end

end

app = Application.new()
