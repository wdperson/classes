require_relative '../models/organization'
require_relative '../application'
require_relative 'spec_helper'

describe Organization do

  before(:each) do
    @root_org = double(:name => "Root Org", :parent => nil)
    @org = double(:name => "Org 1", :parent => "root")
    @child_org = double(:name => "Child 1 Org", :parent => "organization")
  end

  it "should only have one root organization" do
    organization = Organization.new(@root_org.name, @root_org.parent)
    expect(organization.is_root?).to eq(true)
  end

  it "should have just one parent when it is an organization and it should be root " do
    organization = Organization.new(@org.name, @org.parent)
    expect(organization.parent).to eq("root")
  end

  it "should have any number of child organizations when it is an organization" do
    organization = Organization.new(@org.name, @org.parent)
    organization.add_children(2, organization.name)
    expect(organization.children.count).to eq(2)
  end

  it "should not have any children organizations when it is a child organization" do
    organization = Organization.new(@child_org.name, @child_org.parent)
    expect(organization.children.count).to eq(0)
  end

end
