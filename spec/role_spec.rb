require_relative '../models/role.rb'
require_relative '../models/organization.rb'
require_relative '../models/user.rb'

describe Role do

  before(:each) do
    @root_org = double(:name => "Root Org", :parent => nil)
    @org = double(:name => "Org 1", :parent => "root")
    @child_org = double(:name => "Child 1 Org", :parent => "organization")
    @child_org2 = double(:name => "Child 2 Org", :parent => "organization")
    @user = double(:name => "Joe", :role => "user")
    @user2 = double(:name => "Ed", :role => "admin")
  end

  it "should be an admin of all children for an organization" do
    organization = Organization.new(@org.name, @org.parent, @user.name, @user.role)
    organization.add_children(2,"organization", organization.user, organization.role)
    role = Role.new(@user2.name, organization, @user2.role).assign_user
    expect(organization.user).to eq(@user2.name)
    expect(organization.role).to eq(@user2.role)
    expect(organization.children.first.role).to eq("admin")
    expect(organization.children.last.role).to eq("admin")
  end

  it "should have more importance for a role on a child than on a organization" do
    organization = Organization.new(@org.name, @org.parent, @user.name, @user.role)
    organization.add_children(2,"organization", organization.user, "admin")
    Role.new(@user2.name, organization, @user2.role).assign_user
    expect(organization.user).to eq(@user2.name)
    expect(organization.role).to eq(@user2.role)
    expect(organization.children.first.role).to eq("admin")
    expect(organization.children.last.role).to eq("admin")
  end

  it "should deny access to a child when role is denied" do
    organization = Organization.new(@org.name, @org.parent, @user.name, @user.role)
    organization.add_children(2,"organization", organization.user, organization.role)
    organization.add_children(1,"organization", organization.user, 'denied')
    Role.new(@user2.name, organization, @user2.role).assign_user
    expect(organization.user).to eq(@user2.name)
    expect(organization.role).to eq(@user2.role)
    expect(organization.children.first.role).to eq("admin")
    expect(organization.children.last.role).to eq("admin")
  end

  it "should allow access to other children when role is denied on one" do
    organization = Organization.new(@org.name, @org.parent, @user.name, @user.role)
    organization.add_children(2,"organization", organization.user, organization.role)
    Role.new(@user2.name, organization, @user2.role).assign_user
    expect(organization.user).to eq(@user2.name)
    expect(organization.role).to eq(@user2.role)
    expect(organization.children.first.role).to eq("admin")
    expect(organization.children.last.role).to eq("admin")
  end

  it "should allow access to organization if role is denied to one child" do
    organization = Organization.new(@org.name, @org.parent, @user.name, @user.role)
    organization.add_children(2,"organization", organization.user, organization.role)
    Role.new(@user2.name, organization, @user2.role).assign_user
    expect(organization.user).to eq(@user2.name)
    expect(organization.role).to eq(@user2.role)
    expect(organization.children.first.role).to eq("admin")
    expect(organization.children.last.role).to eq("admin")
  end

end
