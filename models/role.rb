class Role

  def initialize(user_name, organization, role)
    @user_name = user_name
    @organization = organization
    @role = role
  end

  def assign_user
    @organization.user = @user_name
    @organization.role = @role
    if @organization.children.size > 0 && @organization.role == "admin"
      @organization.children.each do |child|
        child.role = @role
      end
    end
  end

end
