require 'pry'

class Organization
  attr_accessor :name, :parent, :children, :user, :role

  def initialize(name, parent, user, role)
    @children = []
    @name = name
    @parent = parent
    @user = user
    @role = role
  end

  def create
  end

  def is_root?
    @parent.nil?
  end

  def is_child?
    @parent == "organization"
  end

  def add_children(number, parent, user, role)
    (1..number).each do |idx|
      child = Organization.new("Child #{idx} Org", parent, user, role)
      @children << child
    end
  end


end
